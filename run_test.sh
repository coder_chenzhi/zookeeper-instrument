#!/bin/bash
for index in {1..10}
do
	echo $index
	ant test-core-java  > test_bench_log_$index 2>&1
	tar zcvf test_bench_detail_log_$index.tar.gz build/test/logs
	sleep 5m
done

