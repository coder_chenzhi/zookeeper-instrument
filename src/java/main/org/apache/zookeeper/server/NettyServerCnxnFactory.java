/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.zookeeper.server;

import static org.jboss.netty.buffer.ChannelBuffers.dynamicBuffer;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;

import com.sun.management.ThreadMXBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandler.Sharable;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.channel.WriteCompletionEvent;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

public class NettyServerCnxnFactory extends ServerCnxnFactory {
    Logger LOG = LoggerFactory.getLogger(NettyServerCnxnFactory.class);
    public static final Logger OVERHEAD_LOGGER = LoggerFactory.getLogger("LoggingOverhead");

    ServerBootstrap bootstrap;
    Channel parentChannel;
    ChannelGroup allChannels = new DefaultChannelGroup("zkServerCnxns");
    HashMap<InetAddress, Set<NettyServerCnxn>> ipMap =
        new HashMap<InetAddress, Set<NettyServerCnxn>>( );
    InetSocketAddress localAddress;
    int maxClientCnxns = 60;
    
    /**
     * This is an inner class since we need to extend SimpleChannelHandler, but
     * NettyServerCnxnFactory already extends ServerCnxnFactory. By making it inner
     * this class gets access to the member variables and methods.
     */
    @Sharable
    class CnxnChannelHandler extends SimpleChannelHandler {

        @Override
        public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception
        {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            if (true || LOG.isTraceEnabled()) {
                loggingID = "29eb2c19-fd7f";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.trace("Channel closed " + e);
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }
            allChannels.remove(ctx.getChannel());
        }

        @Override
        public void channelConnected(ChannelHandlerContext ctx,
                ChannelStateEvent e) throws Exception
        {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            if (true || LOG.isTraceEnabled()) {
                loggingID = "0c5a7587-64b6";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.trace("Channel connected " + e);
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }
            allChannels.add(ctx.getChannel());
            NettyServerCnxn cnxn = new NettyServerCnxn(ctx.getChannel(),
                    zkServer, NettyServerCnxnFactory.this);
            ctx.setAttachment(cnxn);
            addCnxn(cnxn);
        }

        @Override
        public void channelDisconnected(ChannelHandlerContext ctx,
                ChannelStateEvent e) throws Exception
        {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            if (true || LOG.isTraceEnabled()) {
                loggingID = "bc356a16-82f5";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.trace("Channel disconnected " + e);
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }
            NettyServerCnxn cnxn = (NettyServerCnxn) ctx.getAttachment();
            if (cnxn != null) {
                if (true || LOG.isTraceEnabled()) {
                    loggingID = "cc4e8dc2-88e7";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.trace("Channel disconnect caused close " + e);
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                }
                cnxn.close();
            }
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
            throws Exception
        {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            LOG.warn("Exception caught " + e, e.getCause());
            NettyServerCnxn cnxn = (NettyServerCnxn) ctx.getAttachment();
            if (cnxn != null) {
                if (true || LOG.isDebugEnabled()) {
                    loggingID = "ca19dd62-40cc";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.debug("Closing " + cnxn);
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                }
                cnxn.close();
            }
        }

        @Override
        public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
            throws Exception
        {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            if (true || LOG.isTraceEnabled()) {
                loggingID = "87efccbe-afc0";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.trace("message received called " + e.getMessage());
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }
            try {
                if (true || LOG.isDebugEnabled()) {
                    loggingID = "6938b92b-aaf0";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.debug("New message " + e.toString()
                            + " from " + ctx.getChannel());
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                }
                NettyServerCnxn cnxn = (NettyServerCnxn)ctx.getAttachment();
                synchronized(cnxn) {
                    processMessage(e, cnxn);
                }
            } catch(Exception ex) {
                LOG.error("Unexpected exception in receive", ex);
                throw ex;
            }
        }

        private void processMessage(MessageEvent e, NettyServerCnxn cnxn) {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            if (true || LOG.isDebugEnabled()) {
                loggingID = "59cad499-a528";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.debug(Long.toHexString(cnxn.sessionId) + " queuedBuffer: "
                        + cnxn.queuedBuffer);
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }

            if (e instanceof NettyServerCnxn.ResumeMessageEvent) {
                loggingID = "c5b1bd0d-84bf";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.debug("Received ResumeMessageEvent");
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));

                if (cnxn.queuedBuffer != null) {
                    if (true || LOG.isTraceEnabled()) {
                        loggingID = "2f187de2-3c98";
                        beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        beforeTime = System.nanoTime();
                        LOG.trace("processing queue "
                                + Long.toHexString(cnxn.sessionId)
                                + " queuedBuffer 0x"
                                + ChannelBuffers.hexDump(cnxn.queuedBuffer));
                        afterTime = System.nanoTime();
                        afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                    }
                    cnxn.receiveMessage(cnxn.queuedBuffer);
                    if (!cnxn.queuedBuffer.readable()) {
                        loggingID = "8aef9db9-5356";
                        beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        beforeTime = System.nanoTime();
                        LOG.debug("Processed queue - no bytes remaining");
                        afterTime = System.nanoTime();
                        afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                        cnxn.queuedBuffer = null;
                    } else {
                        loggingID = "5ea32d1b-ec65";
                        beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        beforeTime = System.nanoTime();
                        LOG.debug("Processed queue - bytes remaining");
                        afterTime = System.nanoTime();
                        afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                    }
                } else {
                    loggingID = "d381e18c-e795";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.debug("queue empty");
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                }
                cnxn.channel.setReadable(true);
            } else {
                ChannelBuffer buf = (ChannelBuffer)e.getMessage();
                if (true || LOG.isTraceEnabled()) {
                    loggingID = "86f51810-bd40";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.trace(Long.toHexString(cnxn.sessionId)
                            + " buf 0x"
                            + ChannelBuffers.hexDump(buf));
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                }
                
                if (cnxn.throttled) {
                    loggingID = "92414361-dd61";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.debug("Received message while throttled");
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));

                    // we are throttled, so we need to queue
                    if (cnxn.queuedBuffer == null) {
                        loggingID = "388bf98d-4b83";
                        beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        beforeTime = System.nanoTime();
                        LOG.debug("allocating queue");
                        afterTime = System.nanoTime();
                        afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));

                        cnxn.queuedBuffer = dynamicBuffer(buf.readableBytes());
                    }
                    cnxn.queuedBuffer.writeBytes(buf);
                    if (true || LOG.isTraceEnabled()) {
                        loggingID = "b630f03f-f4b5";
                        beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        beforeTime = System.nanoTime();
                        LOG.trace(Long.toHexString(cnxn.sessionId)
                                + " queuedBuffer 0x"
                                + ChannelBuffers.hexDump(cnxn.queuedBuffer));
                        afterTime = System.nanoTime();
                        afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                        OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                    }
                } else {
                    loggingID = "20959409-c325";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.debug("not throttled");
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));

                    if (cnxn.queuedBuffer != null) {
                        if (true || LOG.isTraceEnabled()) {
                            loggingID = "2db6ef3a-b69c";
                            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            beforeTime = System.nanoTime();
                            LOG.trace(Long.toHexString(cnxn.sessionId)
                                    + " queuedBuffer 0x"
                                    + ChannelBuffers.hexDump(cnxn.queuedBuffer));
                            afterTime = System.nanoTime();
                            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                        }
                        cnxn.queuedBuffer.writeBytes(buf);
                        if (true || LOG.isTraceEnabled()) {
                            loggingID = "a301da43-5e42";
                            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            beforeTime = System.nanoTime();
                            LOG.trace(Long.toHexString(cnxn.sessionId)
                                    + " queuedBuffer 0x"
                                    + ChannelBuffers.hexDump(cnxn.queuedBuffer));
                            afterTime = System.nanoTime();
                            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                        }

                        cnxn.receiveMessage(cnxn.queuedBuffer);
                        if (!cnxn.queuedBuffer.readable()) {
                            loggingID = "936db11e-0d10";
                            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            beforeTime = System.nanoTime();
                            LOG.debug("Processed queue - no bytes remaining");
                            afterTime = System.nanoTime();
                            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                            cnxn.queuedBuffer = null;
                        } else {
                            loggingID = "fc83ed1c-871b";
                            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            beforeTime = System.nanoTime();
                            LOG.debug("Processed queue - bytes remaining");
                            afterTime = System.nanoTime();
                            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                        }
                    } else {
                        cnxn.receiveMessage(buf);
                        if (buf.readable()) {
                            if (true || LOG.isTraceEnabled()) {
                                loggingID = "efdb1281-eecb";
                                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                                beforeTime = System.nanoTime();
                                LOG.trace("Before copy " + buf);
                                afterTime = System.nanoTime();
                                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                            }
                            cnxn.queuedBuffer = dynamicBuffer(buf.readableBytes()); 
                            cnxn.queuedBuffer.writeBytes(buf);
                            if (true || LOG.isTraceEnabled()) {
                                loggingID = "6324df0a-c714";
                                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                                beforeTime = System.nanoTime();
                                LOG.trace("Copy is " + cnxn.queuedBuffer);
                                afterTime = System.nanoTime();
                                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));

                                loggingID = "be8cff70-4e91";
                                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                                beforeTime = System.nanoTime();
                                LOG.trace(Long.toHexString(cnxn.sessionId)
                                        + " queuedBuffer 0x"
                                        + ChannelBuffers.hexDump(cnxn.queuedBuffer));
                                afterTime = System.nanoTime();
                                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void writeComplete(ChannelHandlerContext ctx,
                WriteCompletionEvent e) throws Exception
        {
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";
            if (true || LOG.isTraceEnabled()) {
                loggingID = "13ae4389-50a4";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.trace("write complete " + e);
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }
        }
        
    }
    
    CnxnChannelHandler channelHandler = new CnxnChannelHandler();
    
    NettyServerCnxnFactory() {
        bootstrap = new ServerBootstrap(
                new NioServerSocketChannelFactory(
                        Executors.newCachedThreadPool(),
                        Executors.newCachedThreadPool()));
        // parent channel
        bootstrap.setOption("reuseAddress", true);
        // child channels
        bootstrap.setOption("child.tcpNoDelay", true);
        /* set socket linger to off, so that socket close does not block */
        bootstrap.setOption("child.soLinger", -1);

        bootstrap.getPipeline().addLast("servercnxnfactory", channelHandler);
    }
    
    @Override
    public void closeAll() {
        ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
        long id = Thread.currentThread().getId();
        long beforeMemUsage = 0;
        long afterMemUsage = 0;
        long beforeTime = 0;
        long afterTime = 0;
        String loggingID = "";

        if (true || LOG.isDebugEnabled()) {
            loggingID = "bad6bbd8-8667";
            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
            beforeTime = System.nanoTime();
            LOG.debug("closeAll()");
            afterTime = System.nanoTime();
            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
        }

        NettyServerCnxn[] allCnxns = null;
        synchronized (cnxns) {
            allCnxns = cnxns.toArray(new NettyServerCnxn[cnxns.size()]);
        }
        // got to clear all the connections that we have in the selector
        for (NettyServerCnxn cnxn : allCnxns) {
            try {
                cnxn.close();
            } catch (Exception e) {
                LOG.warn("Ignoring exception closing cnxn sessionid 0x"
                                + Long.toHexString(cnxn.getSessionId()), e);
            }
        }
        if (true || LOG.isDebugEnabled()) {
            loggingID = "8cdd5bc1-3eeb";
            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
            beforeTime = System.nanoTime();
            LOG.debug("allChannels size:" + allChannels.size() + " cnxns size:"
                    + allCnxns.length);
            afterTime = System.nanoTime();
            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
        }
    }

    @Override
    public void closeSession(long sessionId) {
        ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
        long id = Thread.currentThread().getId();
        long beforeMemUsage = 0;
        long afterMemUsage = 0;
        long beforeTime = 0;
        long afterTime = 0;
        String loggingID = "";

        if (true || LOG.isDebugEnabled()) {
            loggingID = "923fc317-1af2";
            beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
            beforeTime = System.nanoTime();
            LOG.debug("closeSession sessionid:0x" + sessionId);
            afterTime = System.nanoTime();
            afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
            OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                    loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
        }

        NettyServerCnxn cnxn = (NettyServerCnxn) sessionMap.remove(sessionId);
        if (cnxn != null) {
            try {
                cnxn.close();
            } catch (Exception e) {
                LOG.warn("exception during session close", e);
            }
        }
    }

    @Override
    public void configure(InetSocketAddress addr, int maxClientCnxns)
            throws IOException
    {
        configureSaslLogin();
        localAddress = addr;
        this.maxClientCnxns = maxClientCnxns;
    }

    /** {@inheritDoc} */
    public int getMaxClientCnxnsPerHost() {
        return maxClientCnxns;
    }

    /** {@inheritDoc} */
    public void setMaxClientCnxnsPerHost(int max) {
        maxClientCnxns = max;
    }

    @Override
    public int getLocalPort() {
        return localAddress.getPort();
    }

    boolean killed;
    @Override
    public void join() throws InterruptedException {
        synchronized(this) {
            while(!killed) {
                wait();
            }
        }
    }

    @Override
    public void shutdown() {
        LOG.info("shutdown called " + localAddress);
        if (login != null) {
            login.shutdown();
        }
        // null if factory never started
        if (parentChannel != null) {
            parentChannel.close().awaitUninterruptibly();
            closeAll();
            allChannels.close().awaitUninterruptibly();
            bootstrap.releaseExternalResources();
        }

        if (zkServer != null) {
            zkServer.shutdown();
        }
        synchronized(this) {
            killed = true;
            notifyAll();
        }
    }
    
    @Override
    public void start() {
        LOG.info("binding to port " + localAddress);
        parentChannel = bootstrap.bind(localAddress);
    }

    @Override
    public void startup(ZooKeeperServer zks) throws IOException,
            InterruptedException {
        start();
        setZooKeeperServer(zks);
        zks.startdata();
        zks.startup();
    }

    @Override
    public Iterable<ServerCnxn> getConnections() {
        return cnxns;
    }

    @Override
    public InetSocketAddress getLocalAddress() {
        return localAddress;
    }

    private void addCnxn(NettyServerCnxn cnxn) {
        synchronized (cnxns) {
            cnxns.add(cnxn);
            synchronized (ipMap){
                InetAddress addr =
                    ((InetSocketAddress)cnxn.channel.getRemoteAddress())
                        .getAddress();
                Set<NettyServerCnxn> s = ipMap.get(addr);
                if (s == null) {
                    s = new HashSet<NettyServerCnxn>();
                }
                s.add(cnxn);
                ipMap.put(addr,s);
            }
        }
    }

    public void removeCnxn(ServerCnxn cnxn) {
        synchronized(cnxns){
            ThreadMXBean threadMXBean = (ThreadMXBean) ManagementFactory.getThreadMXBean();
            long id = Thread.currentThread().getId();
            long beforeMemUsage = 0;
            long afterMemUsage = 0;
            long beforeTime = 0;
            long afterTime = 0;
            String loggingID = "";

            // if this is not in cnxns then it's already closed
            if (!cnxns.remove(cnxn)) {
                if (true || LOG.isDebugEnabled()) {
                    loggingID = "14a77637-4a06";
                    beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    beforeTime = System.nanoTime();
                    LOG.debug("cnxns size:" + cnxns.size());
                    afterTime = System.nanoTime();
                    afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                    OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                            loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
                }
                return;
            }
            if (true || LOG.isDebugEnabled()) {
                loggingID = "8bf02acc-53e2";
                beforeMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                beforeTime = System.nanoTime();
                LOG.debug("close in progress for sessionid:0x"
                        + Long.toHexString(cnxn.getSessionId()));
                afterTime = System.nanoTime();
                afterMemUsage = threadMXBean.getThreadAllocatedBytes(id);
                OVERHEAD_LOGGER.error("[LoggingOverhead] LoggingID: {}, Time consumption: {} ns, Space consumption: {} bytes",
                        loggingID, (afterTime - beforeTime), (afterMemUsage - beforeMemUsage));
            }

            synchronized (ipMap) {
                Set<NettyServerCnxn> s =
                        ipMap.get(cnxn.getSocketAddress());
                s.remove(cnxn);
            }
        }
    }

}
